
  <!-- Modal -->
  <form method="POST" action="{{ route('spesialis.store') }}" enctype="multipart/form-data">
    @csrf
    @method('GET')
    <div class="modal fade" id="CreatePoli" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CreatePoliLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h1 class="modal-title fs-5" id="CreatePoliLabel">Tambah Poli</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form>
                  <!-- Name input -->
                  <div class="form-outline mb-4">
                      <input type="text" id="nama_poli" class="form-control @error('nama_poli') is-invalid @enderror" name="nama_poli" />
                      <label class="form-label" for="form5Example1">Nama Poli</label>

                      @error('nama_poli')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                
                  <!-- Image input -->    
                  <label class="form-label" for="img_poli">Image Poli</label>
                  <input type="file" class="form-control @error('img_poli') is-invalid @enderror" id="img_poli" name="img_poli" value="{{ old('img_dokter') }}" />

                  @error('img_poli')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </form>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </div>
      </div>
    </div>
  </form>

  <script>
    CKEDITOR.replace( 'content' );
  </script>